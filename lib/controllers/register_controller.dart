import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hindra_login/screens/auth/verify_email.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

import '../screens/auth/auth_screen.dart';
import '../utils/api_endpoints.dart';

class RegisterationController extends GetxController {
  TextEditingController firstNameController = TextEditingController();
  TextEditingController lastNameController = TextEditingController();
  TextEditingController documentTypeController = TextEditingController();
  TextEditingController documentNumberController = TextEditingController();
  TextEditingController dateOfBirthController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController passwordConfirmController = TextEditingController();

  AuthScreen authScreen = Get.put(AuthScreen());

  Future<void> registerWithEmail() async {
    try {
      var headers = {'Content-Type': 'application/json'};
      var url = Uri.parse(
          ApiEndPoints.baseUrl + ApiEndPoints.authEndpoints.registerEmail);
      Map body = {
        'firstName': firstNameController.text,
        'lastName': lastNameController.text,
        'documentType': documentTypeController.text,
        'documentNumber': documentNumberController.text,
        'dateOfBirth': dateOfBirthController.text,
        'email': emailController.text,
        'password': passwordController.text,
        'passwordConfirm': passwordConfirmController.text
      };

      http.Response response =
          await http.post(url, body: jsonEncode(body), headers: headers);

      if (response.statusCode == 201) {
        firstNameController.clear();
        lastNameController.clear();
        documentTypeController.clear();
        documentNumberController.clear();
        dateOfBirthController.clear();
        emailController.clear();
        passwordController.clear();
        passwordConfirmController.clear();
        Get.off(VerifyEmail());
      } else {
        throw jsonDecode(response.body)["message"];
      }
    } catch (e) {
      Get.back();
      showDialog(
          context: Get.context!,
          builder: (context) {
            return SimpleDialog(
              title: Text('Error'),
              contentPadding: const EdgeInsets.all(20),
              children: [Text(e.toString())],
            );
          });
    }
  }
}
