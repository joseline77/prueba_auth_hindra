import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../controllers/login_controller.dart';
import '../../controllers/register_controller.dart';
import 'widgets/input_fields.dart';
import 'widgets/submit_button.dart';

class AuthScreen extends StatefulWidget {
  @override
  State<AuthScreen> createState() => _AuthScreenState();
}
var isRegister = false.obs;
class _AuthScreenState extends State<AuthScreen> {

   LoginController loginController = 
      Get.put(LoginController());
 
  RegisterationController registerationController =
      Get.put(RegisterationController());


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(decoration: 
      const BoxDecoration( 
        gradient: LinearGradient(
          colors: <Color> [
            Color.fromARGB(255, 220, 193, 246),
            Color.fromARGB(255, 255, 255, 255)
          ], begin: Alignment.topCenter
          )),
        child: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(36),
          child: Center(
            child: Obx(
              () => Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                  const  SizedBox(
                      height: 30,
                    ),
                    Container(
                      child: const Text(
                        'WELCOME',
                        style: TextStyle(
                            fontSize: 30,
                            color: Colors.black,
                            fontWeight: FontWeight.w400),
                      ),
                    ),
                 const   SizedBox(
                      height: 20,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        MaterialButton(
                          color: !isRegister.value ? Color.fromARGB(255, 121, 60, 219) : Color.fromARGB(255, 209, 204, 211),
                          onPressed: () {
                            isRegister.value = false;
                          },
                          child: Text('Login'),
                          textColor: Colors.white,
                        ),
                        MaterialButton(
                          color: isRegister.value ? Color.fromARGB(255, 121, 60, 219) : Color.fromARGB(255, 209, 204, 211),
                          onPressed: () {
                            isRegister.value = true;
                          },
                          child: Text('Register'),
                          textColor: Colors.white,

                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 80,
                    ),
                    isRegister.value ? registerWidget() : loginWidget()
                  ]),
            ),
          ),
        ),
      ),
    ),
    );
  }

   Widget loginWidget() {
    return Column(
      children: [
        const SizedBox(
          height: 20,
        ),
        InputTextFieldWidget(loginController.emailController, 'Email address'),
       const SizedBox(
          height: 20,
        ),
        InputTextFieldWidget(loginController.passwordController, 'Password'),
       const SizedBox(
          height: 20,
        ),
        SubmitButton(
          onPressed: () => loginController.loginWithEmail(),
          title: 'Login',
        )
      ],
    );
  }

  Widget registerWidget() {
    return Column(
      children:[
        InputTextFieldWidget(registerationController.firstNameController, 'First Name'),
       const SizedBox(
          height: 20,
        ),
         InputTextFieldWidget(registerationController.lastNameController, 'Last Name'),
       const SizedBox(
          height: 20,
        ),
         InputTextFieldWidget(registerationController.documentTypeController, 'Document Type'),
       const SizedBox(
          height: 20,
        ),
         InputTextFieldWidget(registerationController.documentNumberController, 'Document Number'),
       const SizedBox(
          height: 20,
        ),
        InputTextFieldWidget(registerationController.dateOfBirthController, 'Date Of Birth'),
       const SizedBox(
          height: 20,
        ),
        InputTextFieldWidget(
            registerationController.emailController, 'Email'),
       const SizedBox(
          height: 20,
        ),
        InputTextFieldWidget(
            registerationController.passwordController, 'Password'),
       const SizedBox(
          height: 20,
        ),
         InputTextFieldWidget(registerationController.passwordConfirmController, 'Password Confirm'),
       const SizedBox(
          height: 20,
        ),
        SubmitButton(
          onPressed: () => {
           registerationController.registerWithEmail(),
          },
          title: 'Register',
        )
      ],
    );
  }

  
}