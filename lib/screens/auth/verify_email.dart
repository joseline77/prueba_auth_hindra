import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hindra_login/screens/auth/auth_screen.dart';
import 'package:hindra_login/screens/auth/widgets/submit_button.dart';

class VerifyEmail extends StatefulWidget {
  const VerifyEmail({super.key});

  @override
  State<VerifyEmail> createState() => _VerifyEmailState();
}

class _VerifyEmailState extends State<VerifyEmail> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: new AppBar(
         title: new Text("Verify Email"),
          backgroundColor: Color.fromARGB(255, 121, 60, 219),
        ),
        body: Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text("Enviamos un correo electrónico con un código de verificación",
                style: TextStyle(fontSize: 20, color: Colors.grey),
                textAlign: TextAlign.center),
                SubmitButton(onPressed: () => Get.off(AuthScreen()), title: "Login")
              ],
            ),
            decoration: const BoxDecoration(
                gradient: LinearGradient(colors: <Color>[
              Color.fromARGB(255, 220, 193, 246),
              Color.fromARGB(255, 255, 255, 255)
            ], 
            begin: Alignment.topCenter)
            )
            )
            );
  }
}
