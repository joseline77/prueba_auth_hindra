import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../utils/api_endpoints.dart';
import 'auth/auth_screen.dart';
import 'package:http/http.dart' as http;

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  late Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(actions: [
        TextButton(
            onPressed: () async {
              final SharedPreferences? prefs = await _prefs;
              prefs?.clear();
              Get.offAll(AuthScreen());
            },
            child: Text(
              'logout',
              style: TextStyle(color: Colors.white),
            ))
      ]),
      body: Center(
        child: Column(
          children: [
            Text('Welcome home'),
            TextButton(
                onPressed: () => getMyProfile(),
                  
                child: Text('Mi Usuario'))
          ],
        ),
      ),
    );
  }

  getMyProfile() async {
    final SharedPreferences? prefs = await _prefs;
    var token = (prefs?.get('token'));
    var headers = {'Authorization': 'Bearer $token'};

    var url =
        Uri.parse(ApiEndPoints.baseUrl + ApiEndPoints.authEndpoints.getProfile);

    http.Response res = await http.get(url, headers: headers);

    var jsonData = jsonDecode(res.body);
    if (res.statusCode == 200) {
      print(jsonData['data']['user']);
    } else {
      print(token);
      print(res.statusCode);
      print(jsonData['message']);
    }
  }
}
