class ApiEndPoints {
  static final String baseUrl = 'https://auth-svc-q6ewl.ondigitalocean.app/';
  static _AuthEndPoints authEndpoints = _AuthEndPoints();
}

class _AuthEndPoints {
  final String registerEmail = 'api/auth/register';
  final String loginEmail = 'api/auth/login';
  final String getProfile = 'api/users/me';
}
